<?php

$APP_KEY='sLWtBmYaITN2SiKF' ;
$SESSION_TOKEN='YFj5MBjv/TjmBTArGbyn9hZuZN0n+qY1ef2dMY/OE1k=';

#
# GET ALL EVENTS TYPES
#

$allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);

printf("%-10s %-20s %10s\n", "Id", "Name","Markets") ;

# Sort Events by market size
usort($allEventTypes, function($a, $b) { //Sort the array using a user defined function
    return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
});  

$count=1;
foreach ($allEventTypes as $eventType) {
  printf("%-10s %-20s %10s\n", $eventType->eventType->id ,  $eventType->eventType->name, $eventType->marketCount) ;
  $count++ ;
  # Only show top 3 markets
  if ( $count > 3 ) {
    break ;;
  }
}

#
# GET ALL COMPETITIONS TYPES
#

$allCompetitions = getCompetitions($APP_KEY, $SESSION_TOKEN, "7");

# Sort Events by market size
usort($allCompetitions, function($a, $b) { //Sort the array using a user defined function
    return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
});  


printf("%-10s %-50s %10s %-20s\n", "Id", "Name","Markets", "Region") ;

$count=1;
foreach ($allCompetitions as $CompetitionResult) {
  printf("%-10s %-50s %10s %-20s\n", $CompetitionResult->competition->id, $CompetitionResult->competition->name, $CompetitionResult->marketCount ,  $CompetitionResult->competitionRegion) ;
  $count++ ;
  # Only show top 3 markets
  if ( $count > 1 ) {
    break ;;
  }
}

#
# GET ALL EVENTS
#

$get_events = listEvents($APP_KEY, $SESSION_TOKEN, 10932509);

# Sort Events by market size
usort($get_events, function($a, $b) { //Sort the array using a user defined function
    return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
});  

$count=1;
foreach ($get_events as $event) {
  printf("%-10s %-50s %10s %10s %10s %10s\n", $event->event->id, $event->event->name, $event->event->countryCode,  $event->event->timezone , $event->event->openDate, $event->marketCount );
  $count++ ;
  # Only show top 3 markets
  if ( $count > 3 ) {
    break ;;
  }
}

#
# GET MARKET 
#

$get_market_cat = getMarketCatalog($APP_KEY, $SESSION_TOKEN, 28160700) ;

foreach ($get_market_cat as $market) {
  if ( $market->marketName === "Match Odds" ) {
    printf("%-10s %10s %2.2f\n", $market->marketId,  $market->marketName, $market->totalMatched) ;
  }
}

$market_book = "1.130455739" ;

$get_market_book = getMarketBook($APP_KEY, $SESSION_TOKEN, $market_book) ;

foreach ($get_market_book->runners as $runner) {
    printf("Selection:%-10s ", $runner->selectionId) ;
    $count=1 ;
    foreach ($runner->ex->availableToBack as $availableToBack) {
      if ( $count == 1 ) {
#        echo "Price:" . $availableToBack->price . "(" . dec2frac($availableToBack->price) . ")\n";
        echo "Price:" . $availableToBack->price . "\n";
      }
      $count++ ;
    }
}


#
#  FUNCTIONS
#

function dec2frac($dec) { 

    $decBase = --$dec; 

    $div = 1; 

    do { 

        $div++; 

        $dec = $decBase * $div; 

    } while (intval($dec) != $dec); 

    if ($dec % $div == 0) { 
        $dec = $dec / $div; 
        $div = $div / $div; 
    } 

    return $dec.'/'.$div; 

} 



function getAllCompetitions($appKey, $sessionToken)
{

    $params = '{"filter":{"eventTypeIds":["2"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"10"}' ;

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);

    return $jsonResponse[0]->result;

}


function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);

    return $jsonResponse[0]->result[0];
}

function getMarketCatalog($appKey, $sessionToken, $eventId)
{

    $params = '{"filter":{"eventIds":["' . $eventId . '"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"100"}' ;

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result;

}

function getCompetitions($appKey, $sessionToken, $eventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $eventTypeId . '"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"10"}' ;

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);

    return $jsonResponse[0]->result;

}



function getAllEventTypes($appKey, $sessionToken)
{

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');

    return $jsonResponse[0]->result;

}

function extractSoccerEventTypeId($allEventTypes)
{
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name == 'Soccer') {
            return $eventType->eventType->id;
        }
    }

}

function listEvents($appKey, $sessionToken, $CompetitionId)
{

    $params = '{"filter":{"competitionIds":["' . $CompetitionId . '"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"1"}' ;

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);

    return $jsonResponse[0]->result;
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}

function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

?>
