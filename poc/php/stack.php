<?php

include 'functions.php';
include '/etc/chatbet/session.php';
include 'db.php' ;


$DEBUG = false ;



$payload_value = 7129730 ;
$CompetitionId = 7129730 ;
// get Comp name i.e. UEFA Champions League  from comp id
$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
// Update row with details
//update_bet_comp($user_id, $CompetitionId, $CompetitionName) ;

$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);
//print_r ($get_events) ;
function date_compare($a, $b)
{
	$t1 = strtotime($a->event->openDate);
	$t2 = strtotime($b->event->openDate);
	return $t1 - $t2;
}    

usort($get_events, 'date_compare');




$count=1;
$games = new stdClass();
$text = "Competition: " . $CompetitionName . "\n" ;
$games->text = $CompetitionName ;
$attachments = array() ;

$lastDate = date_create("01/01/1970")  ;
foreach ($get_events as $event) {
	$date = date_create($event->event->openDate);
	$this_date = $date->format('Y-m-d');
	$last_date = $lastDate->format('Y-m-d');
	
    $lastDate = $date ;
//echo "Event: " . $event->event->name . "\n" ;
	$actions = array() ;
	$sports = new stdClass();
	#$date = $event[0]->event->openDate ;
    #$format = "r";
    #echo "Kick Off: " . date_format(date_create($date), $date_format) . "\n";
    $date_format = "D, d M Y";
    $tz = new DateTimeZone('Europe/London');
    $date->setTimezone($tz) ;
    $match_date = date_format($date, $date_format) ;
    
    $date_format = "H:i";
    $tz = new DateTimeZone('Europe/London');
    $date->setTimezone($tz) ;
    $kickoff_date = date_format($date, $date_format) ;
    
    #$kickoff_date = date_format(date_create($event->event->openDate), $date_format) ;
    
	if ($this_date != $last_date) {
		$sports->text = "Date: " . $match_date . "\n" . $event->event->name . "\nKO:" . $kickoff_date;
	} else {
		$sports->text = $event->event->name . "\nKO:" . $kickoff_date ;
    } 
    
	$sports->fallback = "fallback" ;
	$sports->callback_id = $CompetitionId ;
	$sports->color = "#3AA3E3" ;
	$sports->attachment_type = "default" ;
	
	// List all markets for event id
	$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $event->event->id);
	
	// Traverse to find "Match Odds" market
	foreach ($get_market_cap as $market) {
		if ( $market->marketName === "Match Odds" ) {
			$market_book = $market->marketId ;
		}
	}
	
	$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
	
	$sports_array = array() ;
	$sports_item = array() ;
	foreach ($get_market_runners[0]->runners as $runner) {
		# Get team names 
		$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
		# Create team:odds array
		$event_array = array("name"=>$runner->runnerName. ": " . $run_book->runners[0]->lastPriceTraded,
			"value"=>$runner->selectionId . ":" . $runner->runnerName . ":" . $run_book->runners[0]->lastPriceTraded,
			"type"=>"button",
			"text"=>$runner->runnerName. ": " . $run_book->runners[0]->lastPriceTraded
			) ;
	    array_push($actions,$event_array) ;
	}
	
	//$event_array = array("name"=>$event->event->name, "text"=>"AWAY") ;
	//array_push($actions,$event_array) ;
	//$event_array = array("name"=>$event->event->name, "text"=>"DRAW") ;
	//array_push($actions,$event_array) ;
	
	$sports->actions = $actions ;
	array_push($attachments, $sports) ;
    unset($actions) ;
}
$games->attachments = $attachments ;
#print_r ($games) ;
print json_encode($games, JSON_PRETTY_PRINT) ;

?>
