<?php
putenv("CONFIG_DIR=/etc/chatbet") ;
putenv("LOG_DIR=/chatbet/logs") ;
include 'functions.php';
include '/etc/chatbet/session.php';
include 'db.php' ;

#$betId = "90327122836";
#$marketId = "1.130732877";
#$selectionId = "55270" ;
$user_id = "U4Y5AT70R" ;
$user_name = "ash91089" ;
$access_token =  get_access_token($user_id) ;
$refresh_token =  get_refresh_token($user_id) ;

$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
/* Run the following in background so that slack
completes request and doesn't timeout */
ignore_user_abort(true);
set_time_limit(0);
ob_start();
// do initial processing here
$text = '{"text":"Processing...."}' ;

//send_web_hook_response($response_url, $text,0) ;

header('Connection: close');
header('Content-Length: '.ob_get_length());
ob_end_flush();
ob_flush();
flush();

$CompetitionId = 10932509 ;
// get Comp name i.e. UEFA Champions League  from comp id
$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
// Update row with details


$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);

function date_compare($a, $b)
{
	$t1 = strtotime($a->event->openDate);
	$t2 = strtotime($b->event->openDate);
	return $t1 - $t2;
}    

usort($get_events, 'date_compare');

function create_actions_array($name, $value, $runnerName, $odds) {
	return array("name"=>$name,
		"value"=>$value . ":" . $runnerName . ":" . $odds,
		"type"=>"button",
		"text"=>$runnerName. ": " . $odds
		
		) ;
}

$count=1;
$games = new stdClass();
$games->text = $CompetitionName ;
$attachments = array() ;

$count = 1 ;
$lastDate = date_create("01/01/1970")  ;
foreach ($get_events as $event) {
	$date = date_create($event->event->openDate);
	$this_date = $date->format('Y-m-d');
	$last_date = $lastDate->format('Y-m-d');
	
	$lastDate = $date ;
	
	$actions = array() ;
	$sports = new stdClass();
	
	$date_format = "D, d M Y";
	$tz = new DateTimeZone('Europe/London');
	$date->setTimezone($tz) ;
	$match_date = date_format($date, $date_format) ;
	
	$date_format = "H:i";
	$tz = new DateTimeZone('Europe/London');
	$date->setTimezone($tz) ;
	$kickoff_date = date_format($date, $date_format) ;
	
	if ($this_date != $last_date) {
		$sports->text = "Date: " . $match_date . "\n" . $event->event->name . "\nKO:" . $kickoff_date;
	} else {
		$sports->text = $event->event->name . "\nKO:" . $kickoff_date ;
	} 
	
	$sports->fallback = "fallback" ;
	$sports->callback_id = $CompetitionId ;
	$sports->color = "#3AA3E3" ;
	$sports->attachment_type = "default" ;
	
	// List all markets for event id
	$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $event->event->id);
	
	// Traverse to find "Match Odds" market
	foreach ($get_market_cap as $market) {
		if ( $market->marketName === "Match Odds" ) {
			$market_book = $market->marketId ;
		}
	}
	
	$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
	
	$sports_array = array() ;
	$sports_item = array() ;
	foreach ($get_market_runners[0]->runners as $runner) {
		# Get team names 
		$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
		# Create team:odds array
		
		$actions_array = create_actions_array("bet", $runner->selectionId, $runner->runnerName, $run_book->runners[0]->lastPriceTraded) ;
		array_push($actions,$actions_array) ;
	}

	$sports->actions = $actions ;
	array_push($attachments, $sports) ;
	unset($actions) ;
	$count++ ;
	if ($count == 20) {
		//print_r ($attachments)   ;
		break ;
	}
}
$games->attachments = $attachments ;
//print_r ($attachments) ;


//echo "attachements size=" . sizeof($attachments) . "\n" ;
$total_games = sizeof($attachments) ;
$max_attachments = 4 ;
$count=1 ;

$next = array() ;
$next_button = new stdClass() ;
$next_button->text = "Next" ;
$next_button->fallback = "fallback" ;
$next_button->callback_id = $CompetitionId ;
$next_button->color = "#3AA3E3" ;
$next_button->attachment_type = "default" ;

$next_array = create_actions_array("\"next\"", "\"next\"", "\"Next\"", "\"Next\"") ;

array_push($next, $next_array) ;


$next_button->actions = $next ;
//print_r ($next_button) ;


    $new_attach = array() ;
	for( $i = 0; $i<$max_attachments; $i++ ) {
		//print_r (json_encode($attachments[$count-1],JSON_PRETTY_PRINT)) ;
		array_push($new_attach, $attachments[$count-1]) ;
		$count++ ;
        
		if ($count >= $total_games) { break ; }
	}
	
    array_push($new_attach, $next_button) ;
	$games->attachments = $new_attach ;
	$text = json_encode($games,JSON_PRETTY_PRINT) ;
	echo $text ;
	unset($new_attach) ;
	

//print_r($attachments) ;
//$text = json_encode($games,JSON_PRETTY_PRINT) ;
//echo $text ;
//send_web_hook_response($response_url, $text,0) ;

?>

