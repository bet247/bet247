<?php

include 'db.php';
include 'functions.php';

$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;

$DEBUG = false ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	foreach ($_POST as $key => $value) {
		$txt = "Date: " . date('r') . " Key:" . $key . " Value: " . $value ;
		error_log($txt . PHP_EOL, 3, $log_dir . "/bet.txt");
	}
	
	$txt = "Date: " . date('r') . " Command:" . $_POST["text"] . " User: " . $_POST["user_name"] ;
	error_log($txt . PHP_EOL, 3, $log_dir . "/access.txt");
	
	
	
	$user_name =  $_POST["user_name"];
	$user_id =  $_POST["user_id"];
	$channel_id =  $_POST["channel_id"];
	$channel_name =  $_POST["channel_name"];
	$response_url =  $_POST["response_url"];
	$command =  $_POST["command"];
	$team_domain =  $_POST["team_domain"];
	$team_id =  $_POST["team_id"];
	$text =  $_POST["text"];
	$text_parms = explode(" ", $text);
	
	if ( ( $text_parms[0] === "sudo" ) && ( $user_id === "U4Y4570KS" ) ) {
		$user_id = $text_parms[1] ;
		$text = $text_parms[2] ;
	}
	
	// Allow for parameters to the cancel command
	if ( $text_parms[0] === "cancel" ) {
    	$text = "cancel" ;
    	$cancel_parm = $text_parms[1] ;
    }
    
    if ( $text_parms[0] === "unlink" ) {
    	$text = "unlink" ;
    	$unlink_parm = $text_parms[1] ;
    }
    
	$token =  $_POST["token"];
	add_user_session($user_name, $user_id, $channel_id, $channel_name, $response_url, $command, $team_domain, $team_id, $text, $token) ;
	
	switch ($text) {
    case "list-games":
    	# echo's are returned directly to the chat which called list-games
    	echo 'Hello, ' . $user_name ;
    	echo ("\n");
    	echo "This weeks Premier League games:\n" ;
    	
    	# call functions to list markets, events etc from betfair API
    	$allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);
    	$SoccerEventTypeId = extractSoccerEventTypeId($allEventTypes);
    	$nextPremierLeagueGames = getNextUkPremierLeagueGames($APP_KEY, $SESSION_TOKEN, $SoccerEventTypeId);
    	printPremGamesv2($nextPremierLeagueGames);
    	break ;
    case "place":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	header('Content-Type: application/json');
        	$allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);
        	
        	# Sort Events by market size
        	usort($allEventTypes, function($a, $b) { //Sort the array using a user defined function
        			return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
        	});
        	
        	$sports_array = array() ;
        	$sports_item = array() ;
        	$count=1;
        	foreach ($allEventTypes as $eventType) {
        		if ( $eventType->eventType->name === "Soccer" ) {
        			$sport_item = array("name"=>$eventType->eventType->name,"id"=>$eventType->eventType->id) ;
        			array_push($sports_array,$sport_item);
        		}
        		$count++ ;
        		# Only show top 3 markets
        		if ( $count > 5 ) {
        			break ;;
        		}
        	}
        	
        	# Build slack json response for buttons
        	build_slack_response($sports_array,'sport') ;
        }	
    	break ;
    case "log":
    	echo "Audit Log for Bot:\n" ;
    	$myfile = fopen($log_dir . "/access.txt", "r") or die("Unable to open file!");
        echo fread($myfile, filesize($log_dir . "/access.txt")) ;
        fclose($myfile);
        break;
    case "link":
    	# Write out link so we can paste success back to chat
    	$linkfile = fopen($log_dir . "/link.txt", "w") or die("Unable to open file!");
        fwrite($linkfile, $response_url ) ;
        fclose($linkfile);
        
        if ( check_if_registered($user_id) == false ) {
        	error_log($response_url . PHP_EOL, 3, $log_dir . "/access.txt");
                echo "Please click the following link to link your account with betfair.\n" ;
        	echo "<https://identitysso.betfair.com/view/vendor-login?client_id=50248&response_type=code&redirect_uri=signup?user_id=" . $user_id . "|Link Account to BetFair>";
        } else {
        	echo "*You are already registered, if you still have a problem, please contact customer support.*" ;	
        }
        
        break;
    case "unlink":
        
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	if ( $unlink_parm == "confirm" ) {
        		delete_user_details($user_id) ;
        		echo "*We have unlinked your account from Chat Bet*\nYou will also need to go to your Betfair account details page and remove permissions.\nDetails can be found at <https://api.chatbet.co.uk/unlink.htm|this link>" ;
        	} else {
        		echo "*WARNING: This will unlink your betfair account*\nIf you are sure you want to unlink then type `/bet unlink confirm`" ;
        	}
        }
        
        break;
    case "signup":
    	echo "Signup Log for Bot:\n" ;
    	$myfile = fopen($log_dir . "/signup.txt", "r") or die("Unable to open file!");
        echo fread($myfile, filesize($log_dir . "/access.txt")) ;
        fclose($myfile);
        break;
    case "account":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	echo "*Account Status*:\n" ;
        	$access_token =  get_access_token($user_id) ;
        	$refresh_token =  get_refresh_token($user_id) ;
        	
        	$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
        	if ( $new_refresh_token === "FAILED" ) {
        		break ;
        	}
        	
        	#      print_r ($new_refresh_token) ;
        	$account = getAccountDetails($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	echo "*Account Name*: " . $account->firstName . " " . $account->lastName . "\n" ;
        	
        	$funds = getAccountFunds($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	#      print_r ($funds) ;
        	$availableToBetBalance = $funds->availableToBetBalance ;
        	$exposure = $funds->exposure ;
        	echo "*Available to Bet*: £" . number_format((float)$availableToBetBalance, 2, '.', '') . "\n";
        	echo "*Exposure*: £" . number_format((float)$exposure, 2, '.', '') . "\n";
        	echo "*Bet Fair Points*: " . $funds->pointsBalance . "\n";
        }
    	
    	break;
    case "status":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	$access_token =  get_access_token($user_id) ;
        	$refresh_token =  get_refresh_token($user_id) ;
        	
        	$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
        	
        	$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	
        	foreach ($order_list->currentOrders as $currentOrder) {
        		$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId);
        		
        		foreach ($get_market_runners[0]->runners as $runner) {
        			if ($runner->selectionId == $currentOrder->selectionId) {
        				$runnerName = $runner->runnerName ;
        				break ;
        			}
        		}
        		
        		$eventTypeId = getEventTypes($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
        		$competition = getCompetitionNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
        		$event = getEventNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
       		
        		add_unknown_bet_to_archive(
        			$user_id,
        			$user_name,
        			$eventTypeId[0]->eventType->id,
        			$eventTypeId[0]->eventType->name,
        			$competition->competition->id,
        			$competition->competition->name,
        			$event->event->id,
        			$event->event->name,
        			$currentOrder->marketId,
        			$currentOrder->selectionId,
        			$runnerName,
        			$currentOrder->priceSize->price,
        			$currentOrder->priceSize->size,
        			$currentOrder->betId,
        			$currentOrder->side
        			) ;
        	}
        	
        	$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	
        	if (sizeof($order_list->currentOrders) > 0) {
        		echo "*Bet Summary*:\n" ;
        		foreach ($order_list->currentOrders as $order) {
        			display_order_summary($order->betId) ;
        		}
        	} else {
        		echo "No active bets\n" ;
        	}
        	
        }
    	break ;
    case "cancel":
    	echo "Cancel Parm:" . $cancel_parm . "\n" ;
    	// get_bet_market_id($bet_id)
    	break ;
   
    case "help":
    	echo "Help on commands:\n" ;
    	echo "`/bet link` - Link your account to betfair\n" ;
    	echo "`/bet place` - Place a bet\n" ;
    	echo "`/bet status` - Check bet status\n" ;
    	echo "`/bet account` - Check betfair account status\n" ;
    	
    	// get_bet_market_id($bet_id)
    	break ;
   
    case "menu":
    	header('Content-Type: application/json');
        $myfile = fopen("menu.json", "r") or die("Unable to open file!");
           $content =  fread($myfile, filesize("menu.json")) ;
        fclose($myfile);
        echo $content ;
       
    break ;
    case "welcome":
        echo "Welcome to *chatbet!*, the world leading in-chat solution to betting!\n\n" ;
	echo "You are using an invite only beta test solution which is running on the betfair exchange only.\n\n" ;
        echo "Other restrictions:\n\n" ;
        echo "- Limited to Football (Soccer) Competitions\n" ;
        echo "- Limited list of 5 teams per competition\n" ;
        echo "- Minimum bet of £2, Maximum of £4\n" ;
        echo "\n" ;
        echo "Please feedback any issues, improvements, or general comments straight into the chat\n\n" ;
        echo "For a list of commands you can type `/bet help`\n" ;
        echo "\n" ;
        echo "Lets get you started by linking your betfair account, type `/bet link`\n" ;
    break ;
    default:
    	echo "Error: \"" . $text . "\" not a valid command.\n" ;
    }
}


?>
