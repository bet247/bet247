<?php

$APP_KEY='sLWtBmYaITN2SiKF' ;
$SESSION_TOKEN='e+JaXbu63DkwyfMmgbVYEPcuxRRJom1gdcHbgpnMPZ8=' ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  echo 'Hello, ' . $_POST["user_name"] ;
  echo ("\n");
  echo 'Command, ' . $_POST["text"] ;
  echo ("\n");
  echo 'Response Hook, ' . $_POST["response_url"] ;
  echo ("\n");
  if ( $_POST["text"] == "list-games" ) {
    $allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);
    $SoccerEventTypeId = extractSoccerEventTypeId($allEventTypes);
    $nextPremierLeagueGames = getNextUkPremierLeagueGames($APP_KEY, $SESSION_TOKEN, $SoccerEventTypeId);
    printPremGamesv2($nextPremierLeagueGames);
  }
}

function getAllEventTypes($appKey, $sessionToken)
{

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');

    return $jsonResponse[0]->result;

}

function extractSoccerEventTypeId($allEventTypes)
{
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name == 'Soccer') {
            return $eventType->eventType->id;
        }
    }

}

function printPremGamesv2($nextPremierLeagueGames)
{
  foreach ($nextPremierLeagueGames as $event) {
    echo "Game:" . $event->event->name . "\n" ;
  }
}

function getNextUkPremierLeagueGames($appKey, $sessionToken, $SoccerEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $SoccerEventTypeId . '"],
              "marketCountries":["GB"],
              "competitionIds":["10932509"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"1"}' ;

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);

    return $jsonResponse[0]->result;
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}

function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

?>
