<?php

include 'db.php';
include 'functions.php';

$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;

$DEBUG = false ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	#  echo 'Hello, ' . $_POST["user_name"] ;
	echo ("\n");
	
	$myfile = fopen($log_dir . "/chat.txt", "a") or die("Unable to open file!");
	
	foreach ($_POST as $key => $value) {
		$txt = "Date: " . date('r') . " Key:" . $key . " Value: " . $value . "\n";
		fwrite($myfile, $txt) ;
	}
	fclose($myfile);
	
	$payload = json_decode($_POST["payload"]) ;
	
	#  print_r ($payload) ;
	$payload_name = $payload->actions[0]->name ;
	$payload_value = $payload->actions[0]->value ;
	$payload_type = $payload->actions[0]->type ;
	$user_id = $payload->user->id ;
	$user_name = $payload->user->name ;
	
	#  echo "Choice: " . $payload_name . " and ID:" . $payload_value . " and Type:" . $payload_type;
	
	#  echo "Selection: " . $payload_name ;
	
	header('Content-Type: application/json');
	
	switch ($payload_name) {
    case "sport":
    	// for sport payload_value is eventTypeId e.g. 1 = Soccer
    	$eventTypeId = $payload_value ;
    	// get event id name i.e. Soccer from eventTypeId
    	$eventTypeIdName = getEventTypeName($APP_KEY, $SESSION_TOKEN, $eventTypeId);
    	// Update row with details
    	start_bet($user_id, $user_name, $eventTypeId, $eventTypeIdName) ;
    	

    	$allCompetitions = getCompetitions($APP_KEY, $SESSION_TOKEN, $eventTypeId);
    	#  print_r ($allCompetitions) ;
    	
    	# Sort Events by market size
    	usort($allCompetitions, function($a, $b) { //Sort the array using a user defined function
    			return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
    	});
    	
    	$sports_array = array() ;
    	$sports_item = array() ;
    	$count=1;
    	foreach ($allCompetitions as $CompetitionResult) {
    		$sport_item = array("name"=>$CompetitionResult->competition->name,"id"=>$CompetitionResult->competition->id) ;
    		array_push($sports_array,$sport_item);
    		$count++ ;
    		# Only show top 3 markets
    		if ( $count > 5 ) {
    			break ;;
    		}
    	}
    	
    	build_slack_response($sports_array,"competition") ;
    	break ;
    case "competition":
    	// for competition payload_value is CompetitionId  e.g. 228 - UEFA Champions League
    	$CompetitionId = $payload_value ;
    	// get Comp name i.e. UEFA Champions League  from comp id
    	$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
    	// Update row with details
    	update_bet_comp($user_id, $CompetitionId, $CompetitionName) ;
    	
    	$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);
    	
    	/* # Sort Events by market size
    	usort($get_events, function($a, $b) { //Sort the array using a user defined function
    			return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
    	}); */
    	
    	function date_compare($a, $b)
    	{
    		$t1 = strtotime($a->event->openDate);
    		$t2 = strtotime($b->event->openDate);
    		return $t1 - $t2;
    	}    
    	
    	usort($get_events, 'date_compare');
    	
    	$sports_array = array() ;
    	$sports_item = array() ;
    	$count=1;
    	foreach ($get_events as $event) {
    		$sport_item = array("name"=>$event->event->name,"id"=>$event->event->id) ;
    		array_push($sports_array,$sport_item);
    		$count++ ;
    		# Only show top 3 markets
    		if ( $count > 10 ) {
    			break ;;
    		}
    	}
    	
    	build_slack_response($sports_array,"event") ;
    	
    	break ;
    	
    case "event":
    	// for event payload_value is event_id  e.g. 2815611 - Juventus v Barca
    	$eventId = $payload_value ; 
    	// get eEvent name i.e. Juventus v Barca from event id
        $event_name = getEventName($APP_KEY, $SESSION_TOKEN, $eventId) ;
    	// Add details to database
    	update_bet_event($user_id, $eventId, $event_name) ;
    	
    	// List all markets for event id
    	$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $eventId);
    	
    	// Traverse to find "Match Odds" market
    	foreach ($get_market_cap as $market) {
    		if ( $market->marketName === "Match Odds" ) {
    			$market_book = $market->marketId ;
    		}
    	}
    	
    	update_bet_marketid($user_id, $market_book) ;
    	
    	$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
    	
    	$sports_array = array() ;
    	$sports_item = array() ;
    	foreach ($get_market_runners[0]->runners as $runner) {
    		# Get team names 
    		$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
    		# Create team:odds array
    		$sport_item = array("name"=>$runner->runnerName . ": " . $run_book->runners[0]->lastPriceTraded,"id"=>$runner->selectionId . ":" . $runner->runnerName . ":" . $run_book->runners[0]->lastPriceTraded ) ;
    		array_push($sports_array,$sport_item) ;
    	}
    	
    	build_slack_response($sports_array,"bet") ;
    	
    	break ;
    case "bet":
    	$bet = explode(":", $payload_value) ;
    	$bet_selection_id = $bet[0] ;
    	$bet_name = $bet[1] ;
    	$bet_odds = $bet[2] ;
    	update_bet_bet($user_id, $bet_selection_id, $bet_name, $bet_odds) ;
    	$sports_array = array() ;
    	$sports_item = array() ;
    	$sport_item = array("id"=>$bet_odds . ":2","name"=>"£2") ;
    	array_push($sports_array,$sport_item);
    	$sport_item = array("id"=>$bet_odds . ":3","name"=>"£3") ;
    	array_push($sports_array,$sport_item);
    	$sport_item = array("id"=>$bet_odds . ":4","name"=>"£4") ;
    	array_push($sports_array,$sport_item);
    	$txt = get_bet($APP_KEY, $SESSION_TOKEN, $user_id) ;
    	$txt = $txt . "Bet: " . $bet_name . "\nBet Odds:" . $bet_odds . "\n";
    	build_slack_confirm_response($sports_array,"placebet",$bet_odds, $txt) ;
    	break ;
    case "placebet":
    	$placebet = explode(":", $payload_value) ;
    	$placebet_name = $placebet[1] ;
    	$placebet_odds = $placebet[0] ;
    	
    	$marketId = get_market_id($user_id) ;
    	$selectionId = get_selection_id($user_id) ;
    	
    	// echo "marketID:" . $marketId . " : " . $selectionId . "\n" ;
    	
    	$access_token =  get_access_token($user_id) ;
    	$refresh_token =  get_refresh_token($user_id) ;
    	
    	$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
    	$access_token = $new_refresh_token->{'access_token'} ;
    	
    	$bet = placeBet($APP_KEY, $access_token, $marketId, $selectionId, $placebet_name, $placebet_odds) ;
    	
    	if ( $bet->status === "FAILURE" ) {
    		echo "Failure: " ;
    		if ( $bet->errorCode === "INSUFFICIENT_FUNDS" ) {
    			echo "Unable to place bet, you have insufficient funds in your account\n" ;
    	    }
    	    break ;
    	}
    	
    	$bet_orderStatus = $bet->instructionReports[0]->orderStatus ;
    	$bet_sizeMatched = $bet->instructionReports[0]->sizeMatched ;
    	$bet_betId = $bet->instructionReports[0]->betId ;
    	// echo "STATUS:" . $bet_orderStatus . " : " . $bet_sizeMatched ;
    	// Details on status: http://docs.developer.betfair.com/docs/display/1smk3cen4v3lu3yomq5qye0ni/Betting+Enums#BettingEnums-OrderStatus
    	switch ($bet_orderStatus) {
    	case "PENDING":
    		echo "Error: Bet is in a pending state, please contact customer support\n" ;
    		break ;
    	case "EXECUTION_COMPLETE":
    		update_bet_place($user_id, $placebet_name, $bet_betId ) ;
    		display_bet_summary($user_id) ;
    		echo "*Your bet has been placed successfully* \n Stake: £" . $placebet_name . " @ " . $placebet_odds ;
    		break ;
    	case "EXECUTABLE":
    		echo "INFO: BETA TESTING allows a partial bet placement\n" ;
    		update_bet_place($user_id, $placebet_name, $bet_betId ) ;
    		display_bet_summary($user_id) ;
    		echo "*Your bet has been placed successfully* \n Stake: £" . $placebet_name . " @ " . $placebet_odds ;
    		
    		break ;
    	case "EXPIRED":
    		echo "WARNING: It has not been possible to match this bet. Please try again.\n" ;
    		break ;
    	default:
    		echo "Error: An Unknown error has occured, please contact customer support\n" ;
        }
    	
    	break ;
    	
    default:
    	echo "Unknown payload_value:" . $payload_value . "\n";
    	echo "Unknown payload_type:" . $payload_type . "\n";
    	echo "Unknown payload_name:" . $payload_name . "\n";
    }  
    
}

?>
