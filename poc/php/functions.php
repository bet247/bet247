<?php
// CONFIG_DIR variable set in httpd.conf
$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;

function getAllEventTypes($appKey, $sessionToken)
{
	
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');
    
    return $jsonResponse[0]->result;
    
}

function getEventTypes($appKey, $sessionToken, $marketId)
{
    $params = '{"filter":{"marketIds":["' . $marketId . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', $params);
    
    return $jsonResponse[0]->result;
    
}

function extractSoccerEventTypeId($allEventTypes)
{
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name == 'Soccer') {
            return $eventType->eventType->id;
        }
    }
    
}

function printPremGamesv2($nextPremierLeagueGames)
{
	foreach ($nextPremierLeagueGames as $event) {
		$format = "l, j M H:i";
		$start_time = date_format(date_create($event->event->openDate), $format);
		echo "" . $event->event->name . " " . $start_time . "  (id: " . $event->event->id . ")\n" ;
	}
	
}

function getNextUkPremierLeagueGames($appKey, $sessionToken, $SoccerEventTypeId)
{
	
    $params = '{"filter":{"eventTypeIds":["' . $SoccerEventTypeId . '"],
    "marketCountries":["GB"],
    "competitionIds":["10932509"],
    "marketStartTime":{"from":"' . date('c') . '"}},
    "sort":"FIRST_TO_START",
    "maxResults":"1"}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);
    
    return $jsonResponse[0]->result;
}


function build_slack_response($sports_array,$type)
{
	$sports->text = "" ;
	
	$attachments = array() ;
	$attach_item->attachment_type = "default" ;
	$attach_item->color = "#3AA3E3" ;
	$attach_item->callback_id = "cancel" ;
	$attach_item->fallback = "Choice?" ;
	array_push($attachments,$attach_item) ;
	
	$actions = array() ;
	foreach ($sports_array as $sport) {
		$sport_item = array("name"=>$type ,"text"=>$sport['name'],"type"=>"button","value"=>$sport['id']) ;
		array_push($actions,$sport_item) ;
	}
	
	$attach_item->actions = $actions ;
	
	$sports->attachments = $attachments ;
	
	$sports_json = json_encode($sports,JSON_PRETTY_PRINT) ;
	
	echo $sports_json . "\n";
        $log_dir = getenv("LOG_DIR") ;
        $audit_file = fopen($log_dir . "/slack_responses.log", "a") or die("Unable to open file!");
          fwrite($audit_file, date('c') . " build_slack_response: " . $sports_json . "\n" ) ;
        fclose($audit_file);
}

function build_slack_menu_response($options,$type)
{
	
	$sports->text = "Premier League Fixtures" ;
	$sports->response_type = "ephemeral" ;
	
	$attach_item->text = "Choose a game" ;
	$attach_item->attachment_type = "default" ;
	$attach_item->color = "#3AA3E3" ;
	$attach_item->callback_id = "game_selection" ;
	$attach_item->fallback = "fallback" ;
	
	foreach ($options as $option) {
		$option_item = array("text"=>$option['name'],
			"value"=>$option['id']) ;
		array_push($options,$option_item) ;
	}
	
	$action_item->name = "event" ;
	$action_item->text = "Pick a fixture..." ;
	$action_item->type = "select" ;
	$action_item->options = $options ;
	
    
	$actions = array() ;
	array_push($actions,$action_item) ;
	
	$attach_item->actions = $actions ;
	
	$attachments = array() ;
	
	array_push($attachments,$attach_item) ;
	
	$sports->attachments = $attachments ;
	
	$sports_json = json_encode($sports,JSON_PRETTY_PRINT) ;
	
	echo $sports_json . "\n";
        $log_dir = getenv("LOG_DIR") ;
        $audit_file = fopen($log_dir . "/slack_responses.log", "a") or die("Unable to open file!");
          fwrite($audit_file, date('c') . " build_slack_menu_response: " . $sports_json . "\n" ) ;
        fclose($audit_file);
}


function build_slack_confirm_response($sports_array,$type,$callback_id,$text)
{
	$sports->text = "" ;
	
	$attachments = array() ;
	$attach_item->text = $text ;
	$attach_item->attachment_type = "default" ;
	$attach_item->color = "#3AA3E3" ;
	$attach_item->callback_id = $callback_id ;
	$attach_item->fallback = "How much do you want to bet?" ;
	array_push($attachments,$attach_item) ;
	
	$confirm_item->title = "Are you sure?" ;
	$confirm_item->text = "Select to confirm bet?" ;
	$confirm_item->ok_text = "Go for it!" ;
	$confirm_item->dismiss_text = "No, Cancel Please!" ;
	
	$actions = array() ;
	foreach ($sports_array as $sport) {
		$sport_item = array("name"=>$type ,"text"=>$sport['name'],"type"=>"button","value"=>$sport['id'], "confirm"=>$confirm_item) ;
		array_push($actions,$sport_item) ;
	}
	
	$attach_item->actions = $actions ;
	
	$sports->attachments = $attachments ;
	
	$sports_json = json_encode($sports,JSON_PRETTY_PRINT) ;
	
	echo $sports_json . "\n";
        $log_dir = getenv("LOG_DIR") ;
        $audit_file = fopen($log_dir . "/slack_responses.log", "a") or die("Unable to open file!");
          fwrite($audit_file, date('c') . " build_slack_confirm_response: " . $sports_json . "\n" ) ;
        fclose($audit_file);
}

#
#  FUNCTIONS
#

function dec2frac($dec) { 
	
    $decBase = --$dec; 
    
    $div = 1; 
    
    do { 
    	
        $div++; 
        
        $dec = $decBase * $div; 
        
    } while (intval($dec) != $dec); 
    
    if ($dec % $div == 0) { 
        $dec = $dec / $div; 
        $div = $div / $div; 
    } 
    
    return $dec.'/'.$div; 
    
} 



function getAllCompetitions($appKey, $sessionToken)
{
	
    $params = '{"filter":{"eventTypeIds":["2"],
    "marketStartTime":{"from":"' . date('c') . '"}},
    "sort":"FIRST_TO_START",
    "maxResults":"10"}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    
    return $jsonResponse[0]->result;
    
}

function listRunnerBook($appKey, $sessionToken, $marketId, $selectionId)
{
    $params = '{"marketId":"' . $marketId . '",
    "selectionId":"' . $selectionId . '"
    }';
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listRunnerBook', $params);
    
    return $jsonResponse[0]->result[0];
}

function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], 
    "marketProjection":["RUNNER_DESCRIPTION"],
    "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);
    
    return $jsonResponse[0]->result[0];
}

function getMarketRunners($appKey, $sessionToken, $marketId)
{
	
    $params = '{"filter":{"marketIds":["' . $marketId . '"]},
    "marketProjection":["RUNNER_DESCRIPTION"],
    "maxResults":"1" }' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);
    
    return $jsonResponse[0]->result;
    
}

function getMarketCatalog($appKey, $sessionToken, $eventId)
{
	
    $params = '{"filter":{"eventIds":["' . $eventId . '"],
    "marketStartTime":{"from":"' . date('c') . '"}},
    "marketProjection":["RUNNER_DESCRIPTION"],
    "marketTypeCodes":["MATCH_ODDS"],
    "sort":"FIRST_TO_START",
    "maxResults":"100" }' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);
    
    return $jsonResponse[0]->result;
    
}

function getEventTypeName($appKey, $sessionToken, $eventTypeId)
{
	
    $params = '{"filter":{"eventTypeIds":["' . $eventTypeId . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', $params);
    
    return $jsonResponse[0]->result[0]->eventType->name;
}

function getCompetitionNamefromMarket($appKey, $sessionToken, $marketId)
{
	
    $params = '{"filter":{"marketIds":["' . $marketId . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    
    return $jsonResponse[0]->result[0];
    
}

function getCompetitionName($appKey, $sessionToken, $comp_id)
{
	
    $params = '{"filter":{"competitionIds":["' . $comp_id . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    
    return $jsonResponse[0]->result[0]->competition->name;
    
}


function getCompetitions($appKey, $sessionToken, $eventTypeId)
{
	
    $params = '{"filter":{"eventTypeIds":["' . $eventTypeId . '"],
    "marketStartTime":{"from":"' . date('c') . '"}},
    "sort":"FIRST_TO_START"}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    
    return $jsonResponse[0]->result;
    
}

function getEventNamefromMarket($appKey, $sessionToken, $marketId)
{
	
    $params = '{"filter":{"marketIds":["' . $marketId . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);
    
    return $jsonResponse[0]->result[0];
}

function getEventName($appKey, $sessionToken, $event_id)
{
	
    $params = '{"filter":{"eventIds":["' . $event_id . '"]}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);
    
    return $jsonResponse[0]->result[0]->event->name;
}

function getEvent($appKey, $sessionToken, $eventTypeId, $eventId )
{
	
    $params = '{"filter":{"eventTypeIds":["' . $eventTypeId . '"],
    "eventIds":["' . $eventId . '"],
    "maxResults":"1"}}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);
    
    return $jsonResponse[0]->result;
}

function listEvents($appKey, $sessionToken, $CompetitionId)
{
	
    $params = '{"filter":{"competitionIds":["' . $CompetitionId . '"],
    "marketTypeCodes":["MATCH_ODDS"],
    "marketStartTime":{"from":"' . date('c') . '",
    "to":"' . date("c", time() + 604800) . '"}},
    "sort":"FIRST_TO_START",
    "maxResults":"1"}' ;
    
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);
    
    return $jsonResponse[0]->result;
}

function send_web_hook_response($response_url, $text, $delay)
{
	sleep($delay) ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $response_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    	'Content-Type: application/json'
    	));
    
    $postData =
    '{
    "text":"' . $text . '"
    }';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $text);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    return $response ;
    
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    
    $log_dir = getenv("LOG_DIR") ;
    $audit_file = fopen($log_dir . "/audit.log", "a") or die("Unable to open file!");
    fwrite($audit_file, date('c') . " Request: " . $postData . "\n" ) ;
    debug('Post Data: ' . $postData);
    
    $response = json_decode(curl_exec($ch));
    
    fwrite($audit_file, date('c') . " Response: " . json_encode($response) . "\n" ) ;
    debug('Response: ' . json_encode($response));
    
    fclose($audit_file);
    curl_close($ch);
    
    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
    
}

function sportsApingRequest_auth($appKey, $access_token, $operation, $params)
{
	
	$token_type = "BEARER" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    $log_dir = getenv("LOG_DIR") ;
    $audit_file = fopen($log_dir . "/audit.log", "a") or die("Unable to open file!");
    fwrite($audit_file, date('c') . " Request: " . $postData . "\n" ) ;
    
    debug('Post Data: ' . $postData);
    
    $response = json_decode(curl_exec($ch));
    
    fwrite($audit_file, date('c') . " Response: " . json_encode($response) . "\n" ) ;
    debug('Response: ' . json_encode($response));
    
    fclose($audit_file);
    curl_close($ch);
    
    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
    
}

function get_token_refresh($appKey, $sessionToken, $refresh_token)
{
	#   echo "Code:" . $code ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/account/rest/v1.0/token/");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '{ "client_id": "50248", "grant_type": "REFRESH_TOKEN", "refresh_token": "' . $refresh_token . '", "client_secret": "8c8e35bc-c0d1-4665-9bf4-e11eef7fe4ff" }' ;
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    if (isset($response->faultstring)) {
        echo "Status Call Failed, your account may not be registered yet.\nTry linking your account first with \"`/bet247 link`\"\n";
        return "FAILED" ;
    }
    
    return $response;
}

function get_token_request($appKey, $sessionToken, $code)
{
	#   echo "Code:" . $code ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/account/rest/v1.0/token/");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '{ "client_id": "50248", "grant_type": "AUTHORIZATION_CODE", "code": "' . $code . '", "client_secret": "8c8e35bc-c0d1-4665-9bf4-e11eef7fe4ff" }' ;
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    return $response;
}

function getAccountDetails($appKey, $sessionToken, $access_token)
{
    $token_type = "BEARER" ;
    $operation = "getAccountDetails" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/account/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "AccountAPING/v1.0/' . $operation . '", "params" :{}, "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    return $response[0]->result ;
}

function listRunnerBook_auth($appKey, $sessionToken, $access_token, $marketId, $selectionId)
{
    $token_type = "BEARER" ;
    $operation = "listRunnerBook" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $params = '{"marketId":"' . $marketId . '",
    "selectionId":"' . $selectionId . '"
    }';
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    $log_dir = getenv("LOG_DIR") ;
    $audit_file = fopen($log_dir . "/audit.log", "a") or die("Unable to open file!");
    fwrite($audit_file, date('c') . " Request: " . $postData . "\n" ) ;
    
    debug('Post Data: ' . $postData);
    
    $response = json_decode(curl_exec($ch));
    
    fwrite($audit_file, date('c') . " Response: " . json_encode($response) . "\n" ) ;
    debug('Response: ' . json_encode($response));
    
    fclose($audit_file);
    curl_close($ch);
    
    
    
    return $response[0]->result ;
}

function listCurrentOrders($appKey, $sessionToken, $access_token)
{
    $token_type = "BEARER" ;
    $operation = "listCurrentOrders" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :{}, "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    $log_dir = getenv("LOG_DIR") ;
    $audit_file = fopen($log_dir . "/audit.log", "a") or die("Unable to open file!");
    fwrite($audit_file, date('c') . " Request: " . $postData . "\n" ) ;
    
    debug('Post Data: ' . $postData);
    
    $response = json_decode(curl_exec($ch));
    
    fwrite($audit_file, date('c') . " Response: " . json_encode($response) . "\n" ) ;
    debug('Response: ' . json_encode($response));
    
    fclose($audit_file);
    curl_close($ch);
    
    
    
    return $response[0]->result ;
}

function cancelOrders($appKey, $sessionToken, $access_token, $marketId, $betId)
{
    $token_type = "BEARER" ;
    $operation = "cancelOrders" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $params = '{"marketId":"' . $marketId . '",
    "instructions":[{
    "betId":"' . $betId . '",
    "sizeReduction":null
    }]
    }';
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    return $response[0]->result ;
}

function getAccountFunds($appKey, $sessionToken, $access_token)
{
    $token_type = "BEARER" ;
    $operation = "getAccountFunds" ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/account/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'Authorization: ' . $token_type . " " . $access_token,
        'Accept: application/json',
        'Content-Type: application/json'
        ));
    
    $postData =
    '[{ "jsonrpc": "2.0", "method": "AccountAPING/v1.0/' . $operation . '", "params" :{"wallet": "UK"}, "id": 1}]';
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    
    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));
    
    curl_close($ch);
    
    return $response[0]->result ;
}

function placeBet($appKey, $accessToken, $marketId, $selectionId, $size, $price)
{
	
	$timestamp = time() ;
    $params = '{"marketId":"' . $marketId . '",
    "instructions":
    [{"selectionId":"' . $selectionId . '",
    "handicap":"0",
    "side":"BACK",
    "orderType": "LIMIT",
    "limitOrder":{
    "size":"' . $size . '",
    "price":"' . $price . '",
    "persisenceType":"LAPSE",
    "timeInForce":"FILL_OR_KILL"
    }
    }],
    "customerRef":"chatbet_' . $timestamp . '"}';
    
    $jsonResponse = sportsApingRequest_auth($appKey, $accessToken, 'placeOrders', $params);
    
    return $jsonResponse[0]->result;
    
}


function printBetResult($betResult)
{
    echo "Status: " . $betResult->status;
    
    if ($betResult->status == 'FAILURE') {
        echo "\nErrorCode: " . $betResult->errorCode;
        echo "\n\nInstruction Status: " . $betResult->instructionReports[0]->status;
        echo "\nInstruction ErrorCode: " . $betResult->instructionReports[0]->errorCode;
    } else
    echo "Warning!!! Bet placement succeeded !!!";
}




function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}


?>
