<?php

$sports->text = "Choose a Sport" ;

$attachments = array() ;
$attach_item->attachment_type = "default" ;
$attach_item->color = "#3AA3E3" ;
$attach_item->callback_id = "cancel" ;
$attach_item->fallback = "No Choice" ;
array_push($attachments,$attach_item) ;

#$attachments->fallback = "No Choice" ;
#$attachments->callback_id = "cancel" ;
#$attachments->color = "#3AA3E3" ;
#$attachments->attachment_type = "default" ;

$sport_item1->name = "soccer" ;
$sport_item1->text = "Soccer" ;
$sport_item1->type = "button" ;
$sport_item1->value = "soccer" ;

$sport_item2->name = "tennis" ;
$sport_item2->text = "Tennis" ;
$sport_item2->type = "button" ;
$sport_item2->value = "tennis" ;

$sport_item3->name = "horse" ;
$sport_item3->text = "Horse" ;
$sport_item3->type = "button" ;
$sport_item3->value = "horse" ;

$actions = array() ;
array_push($actions,$sport_item1) ;
array_push($actions,$sport_item2) ;
array_push($actions,$sport_item3) ;

$attach_item->actions = $actions ;
#$attachments->actions = $actions ;
#array_push($attachments,$actions) ;

$sports->attachments = $attachments ;

$sports_json = json_encode($sports) ;

echo $sports_json . "\n";

?>
