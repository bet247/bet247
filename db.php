<?php

$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;
include $config_dir . '/db_config.php' ;

function update_bet_place($user_id, $stake, $bet_id){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

    $sql = "UPDATE betfair_trans SET stake=\"$stake\", bet_id=\"" . $bet_id . "\" WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    # Archive current bet transaction to archive table 

    $sql = "INSERT INTO betfair_trans_archive ( slack_user_id, slack_user_name, event_type_id, event_type_name, comp_id, comp_name, event_id, event_name, marketId, selectionId, bet_name, bet_odds, stake, bet_id ) SELECT slack_user_id, slack_user_name, event_type_id, event_type_name, comp_id, comp_name, event_id, event_name, marketId, selectionId, bet_name, bet_odds, stake, bet_id FROM betfair_trans WHERE slack_user_id = \"" . $user_id . "\"";

    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function add_unknown_bet_to_archive($slack_user_id, $slack_user_name, $event_type_id, $event_type_name, $comp_id, $comp_name, $event_id, $event_name, $marketId, $selectionId, $bet_name, $bet_odds, $stake, $bet_id){
	global $servername, $username, $password, $dbname ;
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	
    $sql = "SELECT bet_id from betfair_trans_archive WHERE bet_id=\"" . $bet_id . "\"";
    
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
    	// bet already exists, don't add again
    	
    } else {
    	$sql = "INSERT INTO betfair_trans_archive ( slack_user_id, slack_user_name, event_type_id, event_type_name, comp_id, comp_name, event_id, event_name, marketId, selectionId, bet_name, bet_odds, stake, bet_id, discovered) VALUES (
    	\"" . $slack_user_id . "\",
    	\"" . $slack_user_name . "\" ,
      	\"" . $event_type_id . "\",
    	\"" . $event_type_name . "\",
    	\"" . $comp_id . "\",
    	\"" . $comp_name . "\",
    	\"" . $event_id . "\",
    	\"" . $event_name . "\",
    	\"" . $marketId . "\",
    	\"" . $selectionId . "\",
    	\"" . $bet_name . "\",
    	\"" . $bet_odds . "\",
    	\"" . $stake . "\",
    	\"" . $bet_id  . "\",
    	\"1\")";  
     	//echo "SQL: " . $sql . "\n" ;
     	
    	if ($conn->query($sql) === TRUE) {
    		// Success
    	
    	} else {
    		echo "Error: " . $sql . "<br>" . $conn->error;
    	}  
    }
    
    
    
    $conn->close();
}



function update_bet_bet($user_id, $bet_selection_id, $bet_name, $bet_odds){

global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

    $sql = "UPDATE betfair_trans SET selectionId=\"$bet_selection_id\", bet_name=\"$bet_name\", bet_odds=\"$bet_odds\" WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function update_bet_event($user_id, $event_id, $event_name){

global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


    $sql = "UPDATE betfair_trans SET event_id=$event_id, event_name=\"" . $event_name . "\" WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function get_bet($APP_KEY, $SESSION_KEY, $user_id){

global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


    $sql = "SELECT * FROM betfair_trans WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
    // output data of each row
      while($row = $result->fetch_assoc()) {
#        echo "Sport: " . $row["event_type_id"]. "\nCompetition: " . $row["comp_id"]. "\nEvent " . $row["event_id"]. "\n\n";
        $txt =  "Sport: " . getEventTypeName($APP_KEY, $SESSION_KEY, $row["event_type_id"]) . "\nCompetition: " . getCompetitionName($APP_KEY, $SESSION_KEY, $row["comp_id"]). "\nEvent: " . getEventName($APP_KEY, $SESSION_KEY, $row["event_id"]). "\n";
      }
    } else {
      echo "0 results";
    }

  $conn->close();
  return $txt ;
}

function get_biggest_betters(){
  
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


    $sql = "select slack_user_name, count(1), sum(stake) from betfair_trans_archive group by slack_user_name ORDER BY sum(stake) DESC";
#   echo "SQL:" . $sql . "\n" ;

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
    // output data of each row
    $txt =  "<table border=1>" ;
    $txt = $txt . "<th>Name</th><th>Number of Bets</th><th>Total Amount Bet</th>" ;
      while($row = $result->fetch_assoc()) {
#        echo "Sport: " . $row["event_type_id"]. "\nCompetition: " . $row["comp_id"]. "\nEvent " . $row["event_id"]. "\n\n";
        $txt = $txt .  "<tr><td>" . $row["slack_user_name"] . "</td><td>" . $row["count(1)"] . "</td><td>&pound;" . $row["sum(stake)"] . "</td></tr>";
      }
    } else {
      echo "0 results";
    }
    
    $txt = $txt .  "</table>" ;

  $conn->close();
  return $txt ;
}


function get_total_bets(){
  
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
  $sql = "SELECT * FROM betfair_trans_archive";
  $result = $conn->query($sql);
  
  $rows = $result->num_rows ;
  
  $conn->close();
  return $rows ;
}

function get_total_bet_amount(){

global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

   $sql = "SELECT SUM(stake) FROM betfair_trans_archive" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $sum = ($row["SUM(stake)"]) ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $sum ;
}


function get_total_signup(){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
 
   $sql = "SELECT count(1) FROM user_token" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $sum = ($row["count(1)"]) ;
    }
  } else {
    echo "Error: 0 rows" ;
  }
  
  $conn->close();
  return $sum ;
}


function get_total_slack_cmds(){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
 
   $sql = "SELECT count(1) FROM slack_trans" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $sum = ($row["count(1)"]) ;
    }
  } else {
    echo "Error: 0 rows" ;
  }
  
  $conn->close();
  return $sum ;
}





function get_cmd_total($cmd){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
 
   $sql = "SELECT count(1) FROM slack_trans WHERE text=\"" . $cmd . "\"" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $sum = ($row["count(1)"]) ;
    }
  } else {
    echo "Error: 0 rows" ;
  }
  
  $conn->close();
  return $sum ;
}

function update_bet_comp($user_id, $comp_id, $comp_name){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


    $sql = "UPDATE betfair_trans SET comp_id=$comp_id, comp_name=\"" . $comp_name . "\" WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function update_bet_marketid($user_id, $marketid){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


    $sql = "UPDATE betfair_trans SET marketId=\"" . $marketid . "\" WHERE slack_user_id = \"" . $user_id . "\"";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function start_bet($user_id, $user_name, $event_type_id, $eventTypeIdName){
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


  $sql = "SELECT slack_user_id FROM betfair_trans WHERE slack_user_id=\"" . $user_id . "\"" ;
# echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    $sql = "UPDATE betfair_trans SET slack_user_name=\"" . $user_name . "\", event_type_id=$event_type_id, event_type_name=\"" . $eventTypeIdName . "\" WHERE slack_user_id = \"" . $user_id . "\"";
  } else {
    $sql = "INSERT INTO betfair_trans  (slack_user_id, slack_user_name, event_type_id, event_type_name) VALUES (\"" . $user_id . "\",\"" .  $user_name . "\",\"" .  $event_type_id . "\",\"" .  $eventTypeIdName . "\" )";
  }
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

  $conn->close();
}

function register_user($fname, $sname, $email) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT email FROM user_details WHERE email=\"" . $email . "\"" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    return 1 ; # User already signed up
  } else {
    $sql = "INSERT INTO user_details  (fname, sname, email) VALUES (\"" . $fname . "\",\"" .  $sname . "\",\"" . $email . "\")";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        return 0 ; # Success
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return 2 ; # Failure
    }
  }

  $conn->close();
}

function add_user_details($user_id, $access_token, $refresh_token, $expires_in) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 


  $sql = "SELECT id, user_id FROM user_token WHERE user_id=\"" . $user_id . "\"" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    echo "<h1>You are already signed up</h1>" ;
  } else {
    $sql = "INSERT INTO user_token  (user_id, access_token, refresh_token, expires_in) VALUES (\"" . $user_id . "\",\"" .  $access_token . "\",\"" . $refresh_token . "\",\"" . $expires_in . "\")";
#   echo "SQL:" . $sql . "\n" ;
  
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }

  $conn->close();
}

function delete_user_details($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
  
  
  $sql = "SELECT id, user_id FROM user_token WHERE user_id=\"" . $user_id . "\"" ;
  
  $result = $conn->query($sql) ;
  // echo "SQL:" . $sql . " ROWS Returned:" . $result->num_rows . "\n" ;
  if ($result->num_rows > 0) {
  	  $sql = "DELETE FROM user_token WHERE user_id = \"" . $user_id . "\"";
  	  if ($conn->query($sql) === TRUE) {
  	  	  echo "*Success*\n" ;
  	  } else {
  	  	  echo "Error: " . $sql . "<br>" . $conn->error;
  	  }
  } else {
  	  echo "*Your account is not linked*" ;
  }
  
  $conn->close();
}

function add_user_session($user_name, $user_id, $channel_id, $channel_name, $response_url, $command, $team_domain, $team_id, $text, $token) {
  global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

#  $sql = "SELECT id, user_id FROM slack_trans WHERE user_id=\"" . $user_id . "\"" ;
#  echo "SQL:" . $sql ;
#  $result = $conn->query($sql) ;

#  if ($result->num_rows == 0) {
    $sql = "INSERT INTO slack_trans (user_name, user_id, channel_id, channel_name, response_url, command, team_domain, team_id, text, token ) VALUES (\"" . $user_name . "\",\"" . $user_id . "\",\"" . $channel_id . "\",\"" . $channel_name . "\",\"" . $response_url . "\",\"" . $command . "\",\"" . $team_domain . "\",\"" . $team_id. "\",\"" . $text . "\",\"" . $token . "\")";
#    echo "SQL:" . $sql . "\n" ;
    
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
#  } 

  $conn->close();
}

function add_user($user_name, $user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT id, user_id FROM slack_trans WHERE user_id=\"" . $user_id . "\"" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows == 0) {
    $sql = "INSERT INTO slack_trans (user_name, user_id) VALUES (\"" . $user_name . "\",\"" . $user_id . "\")";
#    echo "SQL:" . $sql . "\n" ;
    
    if ($conn->query($sql) === TRUE) {
        echo " " ;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
  } 

  $conn->close();
}

function get_access_token($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT user_id, access_token FROM user_token  WHERE user_id=\"" . $user_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $access_token = $row["access_token"] ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $access_token ;
}

function get_market_id($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT marketId, slack_user_id FROM betfair_trans  WHERE slack_user_id=\"" . $user_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $access_token = $row["marketId"] ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $access_token ;
}

function get_bet_market_id($bet_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT marketId, bet_id FROM betfair_trans_archive  WHERE bet_id=\"" . $bet_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $access_token = $row["marketId"] ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $access_token ;
}

function get_selection_id($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT selectionId, slack_user_id FROM betfair_trans  WHERE slack_user_id=\"" . $user_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $access_token = $row["selectionId"] ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $access_token ;
}

function check_if_registered($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
  
  $sql = "SELECT user_id FROM user_token  WHERE user_id=\"" . $user_id ."\"" ;
  #  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;
  $conn->close();
  
  if ($result->num_rows > 0) {
  	  
  	  return true ;
  } else {
  	
  	  return false ;
  }
   
}

function get_refresh_token($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT user_id, refresh_token FROM user_token  WHERE user_id=\"" . $user_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $access_token = $row["refresh_token"] ;
    }
  } else {
    echo "Error: 0 rows" ;
  }

  $conn->close();
  return $access_token ;
}

function display_bet_summary($user_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT * FROM betfair_trans WHERE slack_user_id=\"" . $user_id . "\" ORDER BY ID DESC LIMIT 1" ;
#  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "*Bet Summary*:\n" ;
      echo "*Username*: " . $row["slack_user_name"] . "\n" ;
      echo "*Event Type*: " . $row["event_type_name"] . "\n" ;
      echo "*Competition*: " . $row["comp_name"] . "\n" ;
      echo "*Event Name*: " . $row["event_name"] . "\n" ;
      echo "*Bet Selection*: " . $row["bet_name"] . "\n" ;
      echo "*Bet Odds*: " . $row["bet_odds"] . "\n" ;
    }
  } else {
    echo "Error: No Bet Placed" ;
  }

  $conn->close();
}

function display_order_summary($bet_id) {
global $servername, $username, $password, $dbname ;
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT * FROM betfair_trans_archive WHERE bet_id=\"" . $bet_id . "\" ORDER BY ID DESC LIMIT 1" ;
  #  echo "SQL:" . $sql ;
  $result = $conn->query($sql) ;
  
  if ($result->num_rows > 0) {
  	  // output data of each row
  	  while($row = $result->fetch_assoc()) {
  	  	  echo "*ID*: " . $row["id"] . " " ;
  	  	  echo "*Event*: " . $row["event_name"] . " " ;
  	  	  echo "*Selection*: " . $row["bet_name"] . " " ;
  	  	  echo "*Odds*: " . $row["bet_odds"] . " " ;
  	  	  echo "*Stake*: £" . $row["stake"] . " " ;
  	  	  $pot_profit = ($row["stake"] * $row["bet_odds"]) - $row["stake"] ;
  	  	  echo "*Potential Profit*: £" . $pot_profit . "\n" ;
  	  }
  } else {
  	  echo "Error: No Bet Placed\n" ;
  }
  
  $conn->close();
}


?>
