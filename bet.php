<?php

include 'db.php';
include 'functions.php';

$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;

$DEBUG = false ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	foreach ($_POST as $key => $value) {
		$txt = "Date: " . date('r') . " Key:" . $key . " Value: " . $value ;
		error_log($txt . PHP_EOL, 3, $log_dir . "/bet.txt");
	}
	
	$txt = "Date: " . date('r') . " Command:" . $_POST["text"] . " User: " . $_POST["user_name"] ;
	error_log($txt . PHP_EOL, 3, $log_dir . "/access.txt");
	
	
	
	$user_name =  $_POST["user_name"];
	$user_id =  $_POST["user_id"];
	$channel_id =  $_POST["channel_id"];
	$channel_name =  $_POST["channel_name"];
	$response_url =  $_POST["response_url"];
	$command =  $_POST["command"];
	$team_domain =  $_POST["team_domain"];
	$team_id =  $_POST["team_id"];
	$text =  $_POST["text"];
	$text_parms = explode(" ", $text);
	
	if ( ( $text_parms[0] === "sudo" ) && ( $user_id === "U4Y4570KS" ) ) {
		$user_id = $text_parms[1] ;
		$text = $text_parms[2] ;
	}
	
	// Allow for parameters to the cancel command
	if ( $text_parms[0] === "cancel" ) {
    	$text = "cancel" ;
    	$cancel_parm = $text_parms[1] ;
    }
    
    if ( $text_parms[0] === "unlink" ) {
    	$text = "unlink" ;
    	$unlink_parm = $text_parms[1] ;
    }
    
	$token =  $_POST["token"];
	add_user_session($user_name, $user_id, $channel_id, $channel_name, $response_url, $command, $team_domain, $team_id, $text, $token) ;
	
	switch ($text) {
    case "list-games":
    	# echo's are returned directly to the chat which called list-games
    	echo 'Hello, ' . $user_name ;
    	echo ("\n");
    	echo "This weeks Premier League games:\n" ;
    	
    	# call functions to list markets, events etc from betfair API
    	$allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);
    	$SoccerEventTypeId = extractSoccerEventTypeId($allEventTypes);
    	$nextPremierLeagueGames = getNextUkPremierLeagueGames($APP_KEY, $SESSION_TOKEN, $SoccerEventTypeId);
    	printPremGamesv2($nextPremierLeagueGames);
    	break ;
    case "place":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	header('Content-Type: application/json');
        	$allEventTypes = getAllEventTypes($APP_KEY, $SESSION_TOKEN);
        	
        	# Sort Events by market size
        	usort($allEventTypes, function($a, $b) { //Sort the array using a user defined function
        			return $a->marketCount > $b->marketCount ? -1 : 1; //Compare the scores
        	});
        	
        	$sports_array = array() ;
        	$sports_item = array() ;
        	$count=1;
        	foreach ($allEventTypes as $eventType) {
        		if ( $eventType->eventType->name === "Soccer" ) {
        			$sport_item = array("name"=>$eventType->eventType->name,"id"=>$eventType->eventType->id) ;
        			array_push($sports_array,$sport_item);
        		}
        		$count++ ;
        		# Only show top 3 markets
        		if ( $count > 5 ) {
        			break ;;
        		}
        	}
        	
        	# Build slack json response for buttons
        	build_slack_response($sports_array,'sport') ;
        }	
    	break ;
    case "log":
    	echo "Audit Log for Bot:\n" ;
    	$myfile = fopen($log_dir . "/access.txt", "r") or die("Unable to open file!");
        echo fread($myfile, filesize($log_dir . "/access.txt")) ;
        fclose($myfile);
        break;
    case "link":
    	# Write out link so we can paste success back to chat
    	$linkfile = fopen($log_dir . "/link.txt", "w") or die("Unable to open file!");
        fwrite($linkfile, $response_url ) ;
        fclose($linkfile);
        
        if ( check_if_registered($user_id) == false ) {
        	error_log($response_url . PHP_EOL, 3, $log_dir . "/access.txt");
        	echo "<https://identitysso.betfair.com/view/vendor-login?client_id=50248&response_type=code&redirect_uri=signup?user_id=" . $user_id . "|Link Account to BetFair>";
        } else {
        	echo "*You are already registered, if you still have a problem, please contact customer support.*" ;	
        }
        
        break;
    case "unlink":
        
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	if ( $unlink_parm == "confirm" ) {
        		delete_user_details($user_id) ;
        		echo "*We have unlinked your account from Chat Bet*\nYou will also need to go to your Betfair account details page and remove permissions.\nDetails can be found at <https://api.chatbet.co.uk/unlink.htm||this link>" ;
        	} else {
        		echo "*WARNING: This will unlink your betfair account*\nIf you are sure you want to unlink then type `/bet unlink confirm`" ;
        	}
        }
        
        break;
    case "signup":
    	echo "Signup Log for Bot:\n" ;
    	$myfile = fopen($log_dir . "/signup.txt", "r") or die("Unable to open file!");
        echo fread($myfile, filesize($log_dir . "/access.txt")) ;
        fclose($myfile);
        break;
    case "account":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	echo "*Account Status*:\n" ;
        	$access_token =  get_access_token($user_id) ;
        	$refresh_token =  get_refresh_token($user_id) ;
        	
        	$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
        	if ( $new_refresh_token === "FAILED" ) {
        		break ;
        	}
        	
        	#      print_r ($new_refresh_token) ;
        	$account = getAccountDetails($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	echo "*Account Name*: " . $account->firstName . " " . $account->lastName . "\n" ;
        	
        	$funds = getAccountFunds($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	#      print_r ($funds) ;
        	$availableToBetBalance = $funds->availableToBetBalance ;
        	$exposure = $funds->exposure ;
        	echo "*Available to Bet*: £" . number_format((float)$availableToBetBalance, 2, '.', '') . "\n";
        	echo "*Exposure*: £" . number_format((float)$exposure, 2, '.', '') . "\n";
        	echo "*Bet Fair Points*: " . $funds->pointsBalance . "\n";
        }
    	
    	break;
    case "status":
        if ( check_if_registered($user_id) == false ) {
        	echo "*Your account is not linked, you can use `/bet link` to link your account.*" ;
        } else {
        	$access_token =  get_access_token($user_id) ;
        	$refresh_token =  get_refresh_token($user_id) ;
        	
        	$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
        	
        	$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	
        	foreach ($order_list->currentOrders as $currentOrder) {
        		$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId);
        		
        		foreach ($get_market_runners[0]->runners as $runner) {
        			if ($runner->selectionId == $currentOrder->selectionId) {
        				$runnerName = $runner->runnerName ;
        				break ;
        			}
        		}
        		
        		$eventTypeId = getEventTypes($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
        		$competition = getCompetitionNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
        		$event = getEventNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
       		
        		add_unknown_bet_to_archive(
        			$user_id,
        			$user_name,
        			$eventTypeId[0]->eventType->id,
        			$eventTypeId[0]->eventType->name,
        			$competition->competition->id,
        			$competition->competition->name,
        			$event->event->id,
        			$event->event->name,
        			$currentOrder->marketId,
        			$currentOrder->selectionId,
        			$runnerName,
        			$currentOrder->priceSize->price,
        			$currentOrder->priceSize->size,
        			$currentOrder->betId,
        			$currentOrder->side
        			) ;
        	}
        	
        	$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;
        	
        	if (sizeof($order_list->currentOrders) > 0) {
        		echo "*Bet Summary*:\n" ;
        		foreach ($order_list->currentOrders as $order) {
        			display_order_summary($order->betId) ;
        		}
        	} else {
        		echo "No active bets\n" ;
        	}
        	
        }
    	break ;
    case "cancel":
    	echo "Cancel Parm:" . $cancel_parm . "\n" ;
    	// get_bet_market_id($bet_id)
    	break ;
    case "test":
    	
    	/* Run the following in background so that slack
    	completes request and doesn't timeout */
    	ignore_user_abort(true);
    	set_time_limit(0);
     	ob_start();
    	// do initial processing here
    	$text = '{"text":"Processing...."}' ;
    	
    	send_web_hook_response($response_url, $text,0) ;
    	
    	header('Connection: close');
    	header('Content-Length: '.ob_get_length());
    	ob_end_flush();
    	ob_flush();
    	flush();
    	
    	$CompetitionId = 10932509 ;
    	// get Comp name i.e. UEFA Champions League  from comp id
    	$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
    	// Update row with details
    	
    	
    	$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);
    	
    	function date_compare($a, $b)
    	{
    		$t1 = strtotime($a->event->openDate);
    		$t2 = strtotime($b->event->openDate);
    		return $t1 - $t2;
    	}    
    	
    	usort($get_events, 'date_compare');
    	
    	
    	$count=1;
    	$games = new stdClass();
    	$games->text = $CompetitionName ;
    	$attachments = array() ;
    	
    	$count = 1 ;
    	$lastDate = date_create("01/01/1970")  ;
    	foreach ($get_events as $event) {
    		$date = date_create($event->event->openDate);
    		$this_date = $date->format('Y-m-d');
    		$last_date = $lastDate->format('Y-m-d');
    		
    		$lastDate = $date ;
    		
    		$actions = array() ;
    		$sports = new stdClass();
    		
    		$date_format = "D, d M Y";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$match_date = date_format($date, $date_format) ;
    		
    		$date_format = "H:i";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$kickoff_date = date_format($date, $date_format) ;
    		
    		if ($this_date != $last_date) {
    			$sports->text = "Date: " . $match_date . "\n" . $event->event->name . "\nKO:" . $kickoff_date;
    		} else {
    			$sports->text = $event->event->name . "\nKO:" . $kickoff_date ;
    		} 
    		
    		$sports->fallback = "fallback" ;
    		$sports->callback_id = $CompetitionId ;
    		$sports->color = "#3AA3E3" ;
    		$sports->attachment_type = "default" ;
    		
    		// List all markets for event id
    		$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $event->event->id);
    		
    		// Traverse to find "Match Odds" market
    		foreach ($get_market_cap as $market) {
    			if ( $market->marketName === "Match Odds" ) {
    				$market_book = $market->marketId ;
    			}
    		}
    		
    		$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
    		
    		$sports_array = array() ;
    		$sports_item = array() ;
    		foreach ($get_market_runners[0]->runners as $runner) {
    			# Get team names 
    			$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
    			# Create team:odds array
    			$event_array = array("name"=>"bet",
    				"value"=>$runner->selectionId . ":" . $runner->runnerName . ":" . $run_book->runners[0]->lastPriceTraded,
    				"type"=>"button",
    				"text"=>$runner->runnerName. ": " . $run_book->runners[0]->lastPriceTraded,
    				"short"=>true
    				) ;
    			array_push($actions,$event_array) ;
    		}
    		
    		$sports->actions = $actions ;
    		array_push($attachments, $sports) ;
    		unset($actions) ;
    		$count++ ;
    		if ($count == 20) {
    			//print_r ($attachments)   ;
    			break ;
    		}
    	}
    	$games->attachments = $attachments ;
    	
    	$text = json_encode($games,JSON_PRETTY_PRINT) ;
    	send_web_hook_response($response_url, $text,0) ;
    	
    	$linkfile = fopen($log_dir . "/json.txt", "w") or die("Unable to open file!");
        fwrite($linkfile, $text ) ;
        fclose($linkfile);
        
    	//echo "Text: " . $text . "\n" ;
    	//echo "Response URL: " . $response_url . "\n" ;
    	break ;
    case "help":
    	echo "Help on commands:\n" ;
    	echo "`/bet link` - Link your account to betfair\n" ;
    	echo "`/bet place` - Place a bet\n" ;
    	echo "`/bet status` - Check bet status\n" ;
    	echo "`/bet account` - Check betfair account status\n" ;
    	
    	// get_bet_market_id($bet_id)
    	break ;
    	
    case "test2":
    	
    	/* Run the following in background so that slack
    	completes request and doesn't timeout */
    	ignore_user_abort(true);
    	set_time_limit(0);
     	ob_start();
    	// do initial processing here
    	$text = '{"text":"Processing...."}' ;
    	
    	send_web_hook_response($response_url, $text,0) ;
    	
    	header('Connection: close');
    	header('Content-Length: '.ob_get_length());
    	ob_end_flush();
    	ob_flush();
    	flush();
    	
    	//header('Content-Type: application/json');
    	$CompetitionId = 10932509 ;
    	// get Comp name i.e. UEFA Champions League  from comp id
    	$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
    	// Update row with details
    	
    	
    	$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);
    	
    	function date_compare($a, $b)
    	{
    		$t1 = strtotime($a->event->openDate);
    		$t2 = strtotime($b->event->openDate);
    		return $t1 - $t2;
    	}    
    	
    	usort($get_events, 'date_compare');
    	
    	function create_actions_array($name, $value, $runnerName, $odds) {
    		return array("name"=>$name,
    			"value"=>$value . ":" . $runnerName . ":" . $odds,
    			"type"=>"button",
    			"text"=>$runnerName. ": " . $odds
    			) ;
    	}
    	
    	$count=1;
    	$games = new stdClass();
    	$games->text = $CompetitionName ;
    	$attachments = array() ;
    	
    	$count = 1 ;
    	$lastDate = date_create("01/01/1970")  ;
    	foreach ($get_events as $event) {
    		$date = date_create($event->event->openDate);
    		$this_date = $date->format('Y-m-d');
    		$last_date = $lastDate->format('Y-m-d');
    		
    		$lastDate = $date ;
    		
    		$actions = array() ;
    		$sports = new stdClass();
    		
    		$date_format = "D, d M Y";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$match_date = date_format($date, $date_format) ;
    		
    		$date_format = "H:i";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$kickoff_date = date_format($date, $date_format) ;
    		
    		if ($this_date != $last_date) {
    			$sports->text = "Date: " . $match_date . "\n" . $event->event->name . "\nKO:" . $kickoff_date;
    		} else {
    			$sports->text = $event->event->name . "\nKO:" . $kickoff_date ;
    		} 
    		
    		$sports->fallback = "fallback" ;
    		$sports->callback_id = $CompetitionId ;
    		$sports->color = "#3AA3E3" ;
    		$sports->attachment_type = "default" ;
    		
    		// List all markets for event id
    		$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $event->event->id);
    		
    		// Traverse to find "Match Odds" market
    		foreach ($get_market_cap as $market) {
    			if ( $market->marketName === "Match Odds" ) {
    				$market_book = $market->marketId ;
    			}
    		}
    		
    		$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
    		
    		$sports_array = array() ;
    		$sports_item = array() ;
    		foreach ($get_market_runners[0]->runners as $runner) {
    			# Get team names 
    			$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
    			# Create team:odds array
    			
    			$actions_array = create_actions_array("bet", $runner->selectionId, $runner->runnerName, $run_book->runners[0]->lastPriceTraded) ;
    			array_push($actions,$actions_array) ;
    		}
    		
    		$sports->actions = $actions ;
    		array_push($attachments, $sports) ;
    		unset($actions) ;
    		$count++ ;
    		if ($count == 20) {
    			//print_r ($attachments)   ;
    			break ;
    		}
    	}
    	$games->attachments = $attachments ;
    	//echo "attachements size=" . sizeof($attachments) . "\n" ;
    	$total_games = sizeof($attachments) ;
    	$max_attachments = 5 ;
    	$count=1 ;
    	
    	
    	
    	while ( $count <= $total_games) {
    		$new_attach = array() ;
    		for( $i = 0; $i<$max_attachments; $i++ ) {
    			//print_r (json_encode($attachments[$count-1],JSON_PRETTY_PRINT)) ;
    			array_push($new_attach, $attachments[$count-1]) ;
    			$count++ ;
    			
    			if ($count >= $total_games) { break ; }
    		}
    		$games->attachments = $new_attach ;
    		$text = json_encode($games,JSON_PRETTY_PRINT) ;
    		
            send_web_hook_response($response_url, $text,0) ;
    		
    		//echo $text ;
    		$games->text = "" ;
    		unset($new_attach) ;
    		
    	}
    	
    case "test3":
    	
    	/* Run the following in background so that slack
    	completes request and doesn't timeout */
    	ignore_user_abort(true);
    	set_time_limit(0);
     	ob_start();
    	// do initial processing here
    	$text = '{"text":"Processing...."}' ;
    	
    	send_web_hook_response($response_url, $text,0) ;
    	
    	header('Connection: close');
    	header('Content-Length: '.ob_get_length());
    	ob_end_flush();
    	ob_flush();
    	flush();
    	
    	$CompetitionId = 10932509 ;
    	// get Comp name i.e. UEFA Champions League  from comp id
    	$CompetitionName = getCompetitionName($APP_KEY, $SESSION_TOKEN, $CompetitionId ) ;
    	// Update row with details
    	
    	
    	$get_events = listEvents($APP_KEY, $SESSION_TOKEN, $CompetitionId);
    	
    	function date_compare($a, $b)
    	{
    		$t1 = strtotime($a->event->openDate);
    		$t2 = strtotime($b->event->openDate);
    		return $t1 - $t2;
    	}    
    	
    	usort($get_events, 'date_compare');
    	
    	function create_actions_array($name, $value, $runnerName, $odds) {
    		return array("name"=>$name,
    			"value"=>$value . ":" . $runnerName . ":" . $odds,
    			"type"=>"button",
    			"text"=>$runnerName. ": " . $odds
    			
    			) ;
    	}
    	
    	function next_button() {
    			return array("name"=>"Next",
    			"value"=>"Next Events",
    			"type"=>"button",
    			"text"=>"Next Events"
    			) ;
    	}
    	
    	
    	$count=1;
    	$games = new stdClass();
    	$games->text = $CompetitionName ;
    	$attachments = array() ;
    	
    	$count = 1 ;
    	$lastDate = date_create("01/01/1970")  ;
    	foreach ($get_events as $event) {
    		$date = date_create($event->event->openDate);
    		$this_date = $date->format('Y-m-d');
    		$last_date = $lastDate->format('Y-m-d');
    		
    		$lastDate = $date ;
    		
    		$actions = array() ;
    		$sports = new stdClass();
    		
    		$date_format = "D, d M Y";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$match_date = date_format($date, $date_format) ;
    		
    		$date_format = "H:i";
    		$tz = new DateTimeZone('Europe/London');
    		$date->setTimezone($tz) ;
    		$kickoff_date = date_format($date, $date_format) ;
    		
    		if ($this_date != $last_date) {
    			$sports->text = "Date: " . $match_date . "\n" . $event->event->name . "\nKO:" . $kickoff_date;
    		} else {
    			$sports->text = $event->event->name . "\nKO:" . $kickoff_date ;
    		} 
    		
    		$sports->fallback = "fallback" ;
    		$sports->callback_id = $CompetitionId ;
    		$sports->color = "#3AA3E3" ;
    		$sports->attachment_type = "default" ;
    		
    		// List all markets for event id
    		$get_market_cap = getMarketCatalog($APP_KEY, $SESSION_TOKEN, $event->event->id);
    		
    		// Traverse to find "Match Odds" market
    		foreach ($get_market_cap as $market) {
    			if ( $market->marketName === "Match Odds" ) {
    				$market_book = $market->marketId ;
    			}
    		}
    		
    		$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $market_book);
    		
    		$sports_array = array() ;
    		$sports_item = array() ;
    		foreach ($get_market_runners[0]->runners as $runner) {
    			# Get team names 
    			$run_book = listRunnerBook($APP_KEY, $SESSION_TOKEN, $market_book, $runner->selectionId) ;
    			# Create team:odds array
    			
    			$actions_array = create_actions_array("bet", $runner->selectionId, $runner->runnerName, $run_book->runners[0]->lastPriceTraded) ;
    			array_push($actions,$actions_array) ;
    		}
    		
    		$sports->actions = $actions ;
    		array_push($attachments, $sports) ;
    		unset($actions) ;
    		$count++ ;
    		if ($count == 20) {
    			//print_r ($attachments)   ;
    			break ;
    		}
    	}
    	$games->attachments = $attachments ;
    	//print_r ($attachments) ;
    	
    	
    	//echo "attachements size=" . sizeof($attachments) . "\n" ;
    	$total_games = sizeof($attachments) ;
    	$max_attachments = 4 ;
    	$count=1 ;
    	
    	$next = array() ;
    	$next_button = new stdClass() ;
    	$next_button->text = "" ;
    	$next_button->fallback = "fallback" ;
    	$next_button->callback_id = $CompetitionId ;
    	$next_button->color = "#3AA3E3" ;
    	$next_button->attachment_type = "default" ;
    	
    	$next_array = next_button() ;
    	
    	array_push($next, $next_array) ;
    	
    	
    	$next_button->actions = $next ;
    	//print_r ($next_button) ;
    	
    	
    	$new_attach = array() ;
    	for( $i = 0; $i<$max_attachments; $i++ ) {
    		//print_r (json_encode($attachments[$count-1],JSON_PRETTY_PRINT)) ;
    		array_push($new_attach, $attachments[$count-1]) ;
    		$count++ ;
    		
    		if ($count >= $total_games) { break ; }
    	}
    	
    	array_push($new_attach, $next_button) ;
    	$games->attachments = $new_attach ;
    	$text = json_encode($games,JSON_PRETTY_PRINT) ;
    	send_web_hook_response($response_url, $text,0) ;
    		
    	
    	break ; 
    case "menu":
    	header('Content-Type: application/json');
        $myfile = fopen("menu.json", "r") or die("Unable to open file!");
           $content =  fread($myfile, filesize("menu.json")) ;
        fclose($myfile);
        echo $content ;
        echo $content ;
    break ;

    default:
    	echo "Error: \"" . $text . "\" not a valid command.\n" ;
    }
}


?>
