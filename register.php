<?php

include 'db.php';
include 'functions.php';

$config_dir = getenv("CONFIG_DIR") ;
$log_dir = getenv("LOG_DIR") ;

include $config_dir . '/session.php' ;
include $config_dir . '/app_config.php' ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $fname = $_GET["fname"] ;
  $sname = $_GET["sname"] ;
  $email = $_GET["email"] ;

  // Add new user details to the database
  switch (register_user($fname, $sname, $email)) {
    case 0:
      echo "Success" ;
      break ;
    case 1:
      echo "User Already registered" ;
      break ;
    case 2:
      echo "Failure" ;
      break ;
    default:
      echo "Unknown Error Occurred" ;
    break ; 
  }

}

?>

