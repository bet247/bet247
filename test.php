<?php
putenv("CONFIG_DIR=/etc/chatbet") ;
include 'functions.php';
include '/etc/chatbet/session.php';
include 'db.php' ;

#$betId = "90327122836";
#$marketId = "1.130732877";
#$selectionId = "55270" ;
$user_id = "U4Y5AT70R" ;
$user_name = "ash91089" ;
$access_token =  get_access_token($user_id) ;
$refresh_token =  get_refresh_token($user_id) ;

$new_refresh_token = get_token_refresh($APP_KEY, $SESSION_TOKEN, $refresh_token);
$order_list = listCurrentOrders($APP_KEY, $SESSION_TOKEN, $new_refresh_token->{'access_token'}) ;

foreach ($order_list->currentOrders as $currentOrder) {
	$get_market_runners = getMarketRunners($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId);
	
	foreach ($get_market_runners[0]->runners as $runner) {
		if ($runner->selectionId == $currentOrder->selectionId) {
			$runnerName = $runner->runnerName ;
			break ;
		}
	}
	
	$eventTypeId = getEventTypes($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
	$competition = getCompetitionNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;
	$event = getEventNamefromMarket($APP_KEY, $SESSION_TOKEN, $currentOrder->marketId) ;

	/* echo "event_type_id: " . $eventTypeId[0]->eventType->id . "\n"  ;
	echo "event_type_name: " . $eventTypeId[0]->eventType->name . "\n"  ;
    echo "comp_id: " . $competition->competition->id . "\n" ;
	echo "comp_name: " . $competition->competition->name . "\n" ;
	echo "event_id: " . $event->event->id . "\n" ;
	echo "event_name: " . $event->event->name . "\n";
	echo "marketId:" . $currentOrder->marketId . "\n" ;
	echo "selectionId:" . $currentOrder->selectionId . "\n" ;
	echo "bet_name:" . $runnerName . "\n" ;
	echo "bet_odds: " . $currentOrder->priceSize->price . "\n" ;
	echo "stake: " . $currentOrder->priceSize->size . "\n" ;
	echo "bet_id:" . $currentOrder->betId . "\n" ;
	echo "side: " . $currentOrder->side . "\n" ; */
	
	add_unknown_bet_to_archive(
		$user_id,
		$user_name,
		$eventTypeId[0]->eventType->id,
		$eventTypeId[0]->eventType->name,
		$competition->competition->id,
		$competition->competition->name,
		$event->event->id,
		$event->event->name,
		$currentOrder->marketId,
		$currentOrder->selectionId,
		$runnerName,
		$currentOrder->priceSize->price,
		$currentOrder->priceSize->size,
		$currentOrder->betId,
		$currentOrder->side
		) ;
}

?>

